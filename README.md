This is a little script which helps to track your books in an Emacs
org-mode database. Instead of typing in all the data manually, all you
need to do is provide an ISBN number and the relevant information will
be pulled from OpenLibrary.org, converted into an org-mode drawer, and
appended to your database.

You can customize the fields that are added to your records, add
custom fields which are not included in OpenLibrary data, and -- if
needed -- correct OpenLibrary data before commiting them to your
database.
