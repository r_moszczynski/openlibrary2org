#!/usr/bin/env python2
# Copyright (C) 2013 Radoslaw Moszczynski (r.moszczynski@uw.edu.pl)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import codecs
import json
import os
import sys
import tempfile
import urllib


def process_openlibrary_answer(answer, processor):
    """
    Extract the top-level value of OpenLibrary's JSON object and pass it
    to further processing.
    """

    properties = []

    for key, value in answer.values()[0].items():
        processor(key, value, properties)

    return properties


def generate_orgmode_properties(key, value, properties):
    """ Convert OpenLibrary data fields into org-mode properties. """
    
    if key in ignored_fields:
        return
    elif key == "authors":
        i = 1
        for author in value:
            properties.append(":%s_%s:\t%s" % ("AUTHOR",str(i),author["name"]))
            i += 1
    elif key == "classifications":
        for k,v in value.items():
            if k in ignored_fields:
                continue
            i = 1
            for c in v:
                properties.append(":%s_%s:\t%s" % (k.upper(),str(i),c))
                i += 1
    elif key == "cover":
        for cover in value:
            properties.append(":%s_%s:\t%s" % ("COVER",str(cover).upper(),value[cover]))
    elif key == "identifiers":
        for k,v in value.items():
            if k in ignored_fields:
                continue
            i = 1
            for c in v:
                properties.append(":%s_%s:\t%s" % (k.upper(),str(i),c))
                i += 1
    elif key == "publish_places":
        i = 1
        for place in value:
            properties.append(":%s_%s:\t%s" % ("PUBLISH_PLACE",str(i),place["name"]))
            i += 1
    elif key == "publishers":
        i = 1
        for publisher in value:
            properties.append(":%s_%s:\t%s" % ("PUBLISHER",str(i),publisher["name"]))
            i += 1
    elif key == "subjects":
        i = 1
        for subject in value:
            properties.append(":%s_%s:\t%s" % ("SUBJECT",str(i),subject["name"]))
            i += 1
    elif hasattr(value, "items") or hasattr(value, "append"):
        os.remove(tmp.name)
        sys.exit("Unknown compound field: %s. Terminating.\n" % key)
    else:
        if not isinstance(value,unicode):
            value = str(value)
        properties.append(":%s:\t%s" % (key.upper(),value.replace("\r\n"," ")))
        
    
def get_config(file):
    """ Parse user configuration files (lists of ignored and custom fields). """

    lst = []

    try:
        f = open(file,"r")
        for line in f.readlines():
            if line[0] != "#":
                lst.append(line[:-1])
        f.close()    
    except Exception as e:
        os.remove(tmp.name)
        sys.exit("Exception occured when reading file %s: %s. Terminating.\n" % (file, e))
    
    return lst


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-I", "--ISBN", help="ISBN to pull data for",required=True)
    parser.add_argument("-i", "--ignorelist", help="path to file with fields to ignore")
    parser.add_argument("-l", "--level", help="level of org headings to generate, defaults to 2 (**)",default=2)
    parser.add_argument("-f", "--format", help="heading format; 0 for <author, title> or 1 for <title, author>, defaults to 0",default=0,choices=["0","1"])
    parser.add_argument("-n", "--noninteractive", help="noninteractive mode -- you won't be asked to verify/correct data",action="store_true")
    parser.add_argument("-o", "--output", help="path to output file (results will be appended)",required=True)
    parser.add_argument("-p", "--privatefields", help="path to file with private fields to be added")
    parser.add_argument("-u", "--uuid", help="add an UUID to each book record", action="store_true")
    args = parser.parse_args()

    tmp = tempfile.NamedTemporaryFile(delete=False)
    books = codecs.open(args.output,"a","utf-8")   
    query = "https://openlibrary.org/api/books?bibkeys=ISBN:%s&format=json&jscmd=data" % args.ISBN
    
    try:
        answer = json.load(urllib.urlopen(query))
        if answer.keys() == []:
            os.remove(tmp.name)
            sys.exit("OpenLibrary returned an empty record. Terminating.\n")
    except Exception as e:
        sys.exit("Failed to connect to OpenLibrary: %s.\nTerminating.\n" % e)

    if args.ignorelist:
        ignored_fields=get_config(args.ignorelist)
    
    properties = process_openlibrary_answer(answer, generate_orgmode_properties)

    if args.privatefields:
        private = get_config(args.privatefields)
        for p in private: properties.append(p)

    if args.uuid: properties.append(":ID:\t%s" % os.popen('uuidgen').read()[:-1])
    
    for field in sorted(properties):
        tmp.write(field.encode("utf-8")+"\n")
        sys.stdout.write(field+"\n")
    sys.stdout.write("\n")
    tmp.close()

    if not args.noninteractive:
        while True:
            edit = raw_input("Enter 'y' if the drawer is correct. Enter 'n' if you want to edit it manually before saving.\n")
            if edit == "n":
                os.system("%s %s" % (os.getenv("EDITOR","vi"),tmp.name))
                break
            elif edit == "y":
                break

    try:
        if args.format == 0:
            books.write("%s %s, %s\n" % (int(args.level)*"*",filter(lambda x: ":AUTHOR_1" in x, properties)[0].split("\t")[1],filter(lambda x: ":TITLE" in x, properties)[0].split("\t")[1]))
        else:
            books.write("%s %s, %s\n" % (int(args.level)*"*",filter(lambda x: ":TITLE" in x, properties)[0].split("\t")[1],filter(lambda x: ":AUTHOR_1" in x, properties)[0].split("\t")[1]))    
    except IndexError:
        books.write("%s %s\n" % (int(args.level)*"*",filter(lambda x: ":TITLE" in x, properties)[0].split("\t")[1]))

    books.write(":PROPERTIES:\n")
    f = codecs.open(tmp.name,"r","utf-8")
    for line in f.readlines():
        books.write(line)
    books.write(":END:\n")
    books.close()
    f.close()
    os.remove(tmp.name)
